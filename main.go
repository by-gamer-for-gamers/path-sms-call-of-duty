package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	profile := os.Getenv("USERPROFILE")
	documantPath := profile + "\\Documents\\"

	dir, err := os.ReadDir(documantPath)
	if err != nil {
		log.Fatalln(err)
	}

	callOfDir := []string{}
	for _, folder := range dir {
		if folder.IsDir() && strings.HasPrefix(folder.Name(), "Call of Duty") {
			callOfDir = append(callOfDir, folder.Name())
		}

	}

	for _, folder := range callOfDir {
		log.Println("Patching folder: ", folder)
		err = filepath.Walk(documantPath+folder,
			func(path string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				err = os.Chmod(path, 0777)
				if err != nil {
					log.Println(err)
				}
				return nil
			})
		if err != nil {
			log.Println(err)
		}
	}
	fmt.Println("Start Call Of Dutty")
	fmt.Println("Valide your phone number")
	fmt.Println("Restart Call Of Dutty")
	fmt.Println("Good Game !!")
	fmt.Scanln()
}
